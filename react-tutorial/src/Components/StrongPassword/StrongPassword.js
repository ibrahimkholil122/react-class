import React, { Component } from 'react';
class StrongPassword extends Component {
    state = { 
        password:'',
        passwordLength: false,
        containsNumber: false,
        isUppercase: false,
        containsSymbol:false
     }
     //check for numbers
     checkForNumbers = (string) =>{
         let matches = string.match(/\d+/g);
         this.setState({
             containsNumber: matches != null ? true: false
         })
     }
     //check for Uppercase
     checkForUpperCase = (string) =>{
        let matches = string.match(/[A-Z]/);
        this.setState({
            isUppercase: matches != null ? true: false
        })
    }
    //check for Symbol
    checkForSymbol = (string) =>{
        let symbols = new RegExp(/[^A-Z a-z0-9]/);
        this.setState({
            containsSymbol: symbols.test(string)? true: false
        })
    }
     //on change input handaler
     handelerChange = input=> e =>{
        
         let targetValue = e.target.value
         this.checkForNumbers(targetValue);
         this.checkForUpperCase(targetValue);
         this.checkForSymbol(targetValue);
         this.setState({
            [input]: targetValue,
            passwordLength:targetValue.length > 7 ? true: false
         })
         console.log(e.target.value);
     }
//submit handaler 
formSubmit = (e) =>{
  e.preventDefault();
  alert('Form Submitted successfully')
}

    render() { 

        let{
            passwordLength,
            containsNumber,
            isUppercase,
            containsSymbol,
        } = this.state;
        let btnStatus = passwordLength && containsNumber && isUppercase && containsSymbol ? false: true;
        return ( 
            <div className="password-wrapper">
            <form >
                 <label className="d-block bg-primary text-white py-2 mb-0">Password Generator</label>
                <div>
                   <input onChange={this.handelerChange('password')} type="text" className="form-control" placeholder="Generate your password"/>
                </div>
                <div>
                    <div className={passwordLength ? 'green': null}>Contains More than 8 characters</div>
                    <div className={containsNumber ? 'green': null}>Contains numbers</div>
                    <div className={isUppercase ? 'green': null}>Contains UpperCase</div>
                    <div  className={containsSymbol ? 'green': null}>Contains Symbols</div>
                </div>
                <div>
                    <button 
                    onClick={this.formSubmit} 
                    className="btn btn-primary d-block w-100 mt-2" 
                    disabled={btnStatus}> Submit</button>
                </div>
            </form>
            </div>
         );
    }
}
 
export default StrongPassword;