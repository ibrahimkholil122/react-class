import React,{Component} from 'react';
class First extends Component{
    constructor(props) {
        super(props);
        this.state ={
            persons:[
                {name: 'ibrahim khalil',email: 'ibrahimkholil@gmail.com',address:'Dhaka bangladesh'},
                {name: 'ibrahim ',email: 'ibrahim@gmail.com',address:'Dhaka'},
                {name: 'anis',email: 'anis@gmail.com',address:'Dhaka, bangladesh'}
            ]
        }
    }
    render(){
        let persons = [];
        this.state.persons.map((row,index)=> {
            persons.push(
                <div key={index}>
                    <h3>Name: {row.name}</h3>
                    <p>Email: {row.email}</p>
                    <p>Address: {row.address}</p>
                </div>
            );
        })
        return(
            <div className="first-component">
                {persons}
            </div>
        )
}
}
export default First;