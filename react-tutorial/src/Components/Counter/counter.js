import React,{ Component} from 'react';

class Counter extends Component{
    constructor(props) {
        super(props);
        this.state ={
            count:10
        }
    }
    increase = () =>{
        this.setState({
            count:this.state.count + 1
        })

    }
    decrease = () =>{
        this.setState({
            count:this.state.count - 1
        })
    }

    render() {
        return(
            <div>
                <h3>Counter Increase and decrease</h3>
                <hr/>
                <div>
                    <button onClick={this.decrease} style={{marginRight:10}}>-</button>
                    <span>{ this.state.count} </span>
                    <button onClick={this.increase} style={{marginLeft:10}}>+</button>
                </div>
            </div>
        )
    }
}
export default Counter;