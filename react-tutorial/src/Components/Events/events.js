import React,{Component} from 'react';


class EventHandeler extends Component{
    state = {
        name:''
    }

    clickHandeler = ()=>{

    }
    inputHandeler = (e)=>{
     this.setState({
         name:e.target.value
     })
    }
    render() {
        return(
            <div className="container">
                <div className="row py-3">
                    <div className="col">
                        <form action="" className="row">
                            <div className="col">
                                <input onChange={this.inputHandeler} type="text" className="form-control" placeholder="Enter your name"/>
                                <div className="results text-left">
                                    <p>
                                        Your Name is: {
                                        this.state.name?  <strong>{this.state.name}</strong>:''
                                        }
                                    </p>

                                </div>
                            </div>
                           <div className="col">
                               <button onClick={this.clickHandeler} className="btn btn-danger">Click</button>
                           </div>
                        </form>

                    </div>
                </div>
            </div>
        )
    }
}
export default EventHandeler;