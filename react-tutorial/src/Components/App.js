import React, { Component} from 'react';
import './App.css';
import First from "./First/First";
import Counter from "./Counter/counter";
import EventHandeler from "./Events/events";
import StrongPassword from "./StrongPassword/StrongPassword";

import 'bootstrap/dist/css/bootstrap.min.css'
import Header from "./Header/Header"
class App extends Component {


    render() {
        return (
            <div className="App">
                <Header/>
                {/*<div className="first_component">*/}
                {/*    <First />*/}
                {/*</div>*/}
                <div className="counter">
                    <Counter/>
                </div>
                <div className="events-component">
                    <h3>Event component data</h3>
                    <EventHandeler/>
                </div>
                <StrongPassword/>
            </div>

        );
    }

}

export default App;
